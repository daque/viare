// Core of the game

import std.stdio;
import std.file;
import std.ascii;

import viare.perspectivetest;
import viare.heightmaptest;

import daque.math.geometry;

void main()
{
	geometryTest();
	heightMapTest();
}
